import java.util.*;

public class Pokemon {

    public static void main(String[] args) {
        String name;
        int pickmon;
        int select;
        int change;
        boolean eventChooesPokemon = true;
        boolean event = true;
        boolean eventChangeName = true;
        Fire fire = new Fire();
        Water water = new Water();
        Grass grass = new Grass();
        Player trainer1 = new Player();
        String currentPokemon = "";
        String pokemonName = "";

        Scanner in = new Scanner(System.in);
        System.out.print("Enter your name :");
        name = in.next();
        System.out.println("Welcome to Pokémon Center..." + name);
        System.out.println("So! You want the Pokémon\n");

        while (eventChooesPokemon == true) {
            System.out.println("**************************");
            System.out.println("Choose Your Pokémon");
            System.out.println("\t1. Charmander");
            System.out.println("\t2. Squirtle");
            System.out.println("\t3. Bulbasaur");
            System.out.println("**************************");
            // event pick bar
            Scanner in1 = new Scanner(System.in);
            System.out.print("Enter Pokémon :: ");
            pickmon = in1.nextInt();
            // pick
            if (pickmon == 1) {
                fire.print();
                currentPokemon = "Charmander";
            } else if (pickmon == 2) {
                water.print();
                currentPokemon = "Squirtle";
            } else if (pickmon == 3) {
                grass.print();
                currentPokemon = "Bulbasaur";
            }
            break;

        }
        while (eventChangeName == true) {
            System.out.print("Hey " + name);
            System.out.println("DO YOU WANT TO CHANGENAME? " + currentPokemon);
            System.out.println("\t1. YES");
            System.out.println("\t2. NO");
            Scanner in3 = new Scanner(System.in);
            System.out.print("Enter Event :: ");
            change = in3.nextInt();
            if (change == 1) {
                Scanner in4 = new Scanner(System.in);
                System.out.print("CHANGENAME Pokémon :: ");
                pokemonName = in4.next();
            }

            break;

        }

        while (event == true) {
            System.out.println("==========================");
            System.out.println("\t1 . STATUS");
            System.out.println("\t2 . ACCTACT");
            System.out.println("\t3 . BERRY");
            System.out.println("\t4 . Rest");
            System.out.println("\t5 . End game  or another");
            System.out.println("==========================");
            // event bar
            Scanner in2 = new Scanner(System.in);
            System.out.println("Enter Event");
            select = in2.nextInt();

            if (select == 1) {
                trainer1.status(name, currentPokemon, pokemonName);
            }
        }

    }

}
