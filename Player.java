public class Player {
    protected int hp;
    protected double attackDamage;
    protected double magicDamage;
    protected String Class;

    public Player() {
        Class = "trainer";
        hp = 100;
        attackDamage = 10;
        magicDamage = 10;
    }

    public void status(String name, String currentPokemon, String pokemonName) {
        System.out.println("Name :" + name);
        System.out.println("Class :" + Class);
        System.out.println("Pokémon :" + currentPokemon);
        System.out.println("Pokémon Name :" + pokemonName);
        System.out.println("");
    }

}
